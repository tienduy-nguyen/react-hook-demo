export const slugify = (str: string) => {
  return str?.toLocaleLowerCase()?.replace(/\s/g, '');
};
