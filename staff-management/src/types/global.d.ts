declare module NodeJS {
  interface ProcessEnv {
    NODE_ENV: 'development' | 'production' | 'test';
    MONGODB_URI: string;
    MONGODB_DB: string;
  }

  interface Global {
    mongo: any;
  }
}
