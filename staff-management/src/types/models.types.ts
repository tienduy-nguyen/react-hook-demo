export enum Gender {
  male = 'Male',
  female = 'Female',
  undisclosed = 'Undisclosed',
}

export interface IStaff {
  _id: string;
  full_name: string;
  birth_day: Date;
  gender: Gender;
  phone_number: string;
  address: string;
  province: string;
  position: string;
  department: string;
}

export interface IPosition {
  _id: string;
  name: string;
}

export interface IDepartment extends Document {
  _id: string;
  name: string;
}
