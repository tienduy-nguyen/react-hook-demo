import { ChakraProvider } from '@chakra-ui/react';
import React from 'react';
import { Layout } from '../components/Layout';
import { SearchProvider } from '../context/searchContext';
import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider>
      <SearchProvider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </SearchProvider>
    </ChakraProvider>
  );
}

export default MyApp;
