import React, { useContext } from 'react';
import {
  Box,
  Text,
  Flex,
  Spacer,
  TableCaption,
  Table,
  Thead,
  Tr,
  Th,
  Td,
  Tbody,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Button,
} from '@chakra-ui/react';
import { connectToDatabase } from '../utils/dbConnect';
import { ChevronDownIcon } from '@chakra-ui/icons';
import { useRouter } from 'next/router';
import { SearchContext } from 'src/context/searchContext';
import Head from 'next/head';
import { Pagination } from 'src/components/Pagination';
import { IDepartment, IPosition, IStaff } from 'src/types/models.types';
import { slugify } from 'src/utils/slugify';

interface Props {
  staffs: IStaff[];
  positions: IPosition[];
  departments: IDepartment[];
}

const Home: React.FC<Props> = ({ staffs, departments, positions }) => {
  const { search } = useContext(SearchContext);
  const router = useRouter();
  const p = parseInt(router.query.p as string) || 1;
  const positionQuery: string = router.query.position as string;
  const departmentQuery: string = router.query.department as string;
  let numberOrder = (p - 1) * 15 + 1;
  const perPage = 15;

  let filteredStaffs = staffs;
  if (search) {
    filteredStaffs = filteredStaffs.filter((staff) => {
      return staff.full_name.toLowerCase().indexOf(search.toLowerCase()) > -1;
    });
  }
  if (departmentQuery) {
    const department_id = departments.find(
      (item) => slugify(item.name) == slugify(departmentQuery)
    )?._id;
    filteredStaffs = filteredStaffs.filter((staff) => {
      return staff.department == department_id;
    });
  }

  if (positionQuery) {
    const position_id = positions.find(
      (item) => slugify(item.name) == slugify(positionQuery)
    )?._id;
    filteredStaffs = filteredStaffs.filter((staff) => {
      return staff.position == position_id;
    });
  }

  // Slice by page
  const displayStaffs = filteredStaffs.slice((p - 1) * perPage, p * perPage);

  return (
    <Box p='1rem'>
      <Head>
        <title>HR Management</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Flex>
        <Text fontSize='2rem' fontWeight='bold'>
          Staff
        </Text>
        <Spacer />
        {/* Filter Department */}
        <Menu>
          <MenuButton h='2.2rem' as={Button} rightIcon={<ChevronDownIcon />}>
            Department
          </MenuButton>
          <MenuList>
            <MenuItem onClick={() => router.push('/')}>All</MenuItem>
            {departments.map((item) => (
              <MenuItem
                key={item._id}
                onClick={() =>
                  router.push(`/?p=1&department=${slugify(item.name)}`)
                }
              >
                {item.name}
              </MenuItem>
            ))}
          </MenuList>
        </Menu>
        {/* Filter Position */}
        <Menu>
          <MenuButton
            h='2.2rem'
            as={Button}
            margin='0 2rem'
            rightIcon={<ChevronDownIcon />}
          >
            Position
          </MenuButton>
          <MenuList>
            <MenuItem onClick={() => router.push('/')}>All</MenuItem>
            {positions.map((item) => (
              <MenuItem
                key={item._id}
                onClick={() =>
                  router.push(`/?p=1&position=${slugify(item.name)}`)
                }
              >
                {item.name}
              </MenuItem>
            ))}
          </MenuList>
        </Menu>
      </Flex>

      <Table variant='simple' size='sm'>
        <TableCaption>Staff</TableCaption>
        <Thead>
          <Tr>
            <Th>No.</Th>
            <Th>Full name</Th>
            <Th>Birth day</Th>
            <Th>Gender</Th>
            <Th>Position</Th>
            <Th>Department</Th>
            <Th>Phone number</Th>
            <Th>Address</Th>
            <Th>City</Th>
          </Tr>
        </Thead>
        <Tbody>
          {displayStaffs.map((staff) => (
            <Tr key={staff._id}>
              <Td>{numberOrder++}</Td>
              <Td>{staff.full_name}</Td>
              <Td>{staff.birth_day}</Td>
              <Td>{staff.gender}</Td>
              <Td>
                {positions.find((item) => item._id === staff.position)?.name}
              </Td>
              <Td>
                {departments.find((item) => item._id == staff.department)?.name}
              </Td>
              <Td>{staff.phone_number}</Td>
              <Td>{staff.address}</Td>
              <Td>{staff.province}</Td>
            </Tr>
          ))}
        </Tbody>
      </Table>

      <Pagination
        p={p}
        total={filteredStaffs.length}
        perPage={perPage}
        department={departmentQuery}
        position={positionQuery}
      />
    </Box>
  );
};

export default Home;

export async function getServerSideProps(): Promise<{ props: Props }> {
  const { db } = await connectToDatabase();
  const rawStaffs = await db.collection('staffs').find({}).toArray();
  const rawPositions = await db.collection('positions').find({}).toArray();
  const rawDepartments = await db.collection('departments').find({}).toArray();

  const staffs: IStaff[] = JSON.parse(JSON.stringify(rawStaffs));
  const departments: IDepartment[] = JSON.parse(JSON.stringify(rawDepartments));
  const positions: IPosition[] = JSON.parse(JSON.stringify(rawPositions));

  return {
    props: {
      staffs,
      departments,
      positions,
    },
  };
}
