import React, { createContext, useState } from 'react';

export interface ISearchContext {
  search: string;
  handleSearch: (search: string) => void;
}
export const SearchContext = createContext<ISearchContext | null>(null);

export const SearchProvider: React.FC<React.ReactNode> = ({ children }) => {
  const [search, setSearch] = useState('');
  const handleSearch = (search: string) => {
    console.log(search);
    setSearch(search);
  };
  return (
    <SearchContext.Provider value={{ search, handleSearch }}>
      {children}
    </SearchContext.Provider>
  );
};
