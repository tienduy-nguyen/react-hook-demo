import { useRouter } from 'next/router';
import { Center, Flex } from '@chakra-ui/react';
import { ArrowRightIcon, ArrowLeftIcon } from '@chakra-ui/icons';

interface Props {
  p: number;
  total: number;
  perPage: number;
  position: string;
  department: string;
}

export const Pagination: React.FC<Props> = ({
  p,
  total,
  perPage,
  position,
  department,
}) => {
  const router = useRouter();
  const currentPage = p;
  const pages = Math.ceil(total / perPage);
  const range = (start: number, stop: number, step: number) =>
    Array.from(
      { length: (stop - start) / step + 1 },
      (_, i) => start + i * step
    );

  const onGotoPage = (p: number, position: string, department: string) => {
    router.push(
      `/?p=${p}${position ? `&position=${position}` : ''}${
        department ? `&department=${department}` : ''
      }`
    );
  };
  return (
    <Center>
      <Flex>
        <Center
          onClick={() => onGotoPage(1, position, department)}
          cursor='pointer'
          w='2.2rem'
          h='2.2rem'
          borderRadius='0.5rem'
          _hover={{ border: '1px solid #909193' }}
          m='0 0.2rem'
        >
          1
        </Center>
        {/* Icon prev */}
        {currentPage > 4 && pages > 5 && (
          <Center
            onClick={() => onGotoPage(currentPage - 3, position, department)}
            cursor='pointer'
            w='2.2rem'
            h='2.2rem'
            borderRadius='0.5rem'
            _hover={{ border: '1px solid #909193' }}
            m='0 0.2rem'
          >
            <ArrowLeftIcon />
          </Center>
        )}
        {range(
          currentPage >= 4 ? currentPage - 2 : 2,
          currentPage + 2 >= pages - 1 ? pages - 1 : currentPage + 2,
          1
        ).map((n) => (
          <Center
            onClick={() => onGotoPage(n, position, department)}
            key={n}
            cursor='pointer'
            w='2.2rem'
            h='2.2rem'
            borderRadius='0.5rem'
            _hover={{ border: '1px solid #909193' }}
            m='0 0.2rem'
          >
            {n}
          </Center>
        ))}

        {/* Icon next */}
        {currentPage <= pages - 4 && pages > 5 && (
          <Center
            onClick={() => onGotoPage(currentPage + 3, position, department)}
            cursor='pointer'
            w='2.2rem'
            h='2.2rem'
            borderRadius='0.5rem'
            _hover={{ border: '1px solid #909193' }}
            m='0 0.2rem'
          >
            <ArrowRightIcon />
          </Center>
        )}
        {pages !== 1 ? (
          <Center
            onClick={() => onGotoPage(pages, position, department)}
            cursor='pointer'
            w='2.2rem'
            h='2.2rem'
            borderRadius='0.5rem'
            _hover={{ border: '1px solid #909193' }}
            m='0 0.2rem'
          >
            {pages}
          </Center>
        ) : (
          ''
        )}
      </Flex>
    </Center>
  );
};
