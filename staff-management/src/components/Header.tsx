import {
  GridItem,
  Input,
  InputGroup,
  InputLeftElement,
} from '@chakra-ui/react';
import { SearchIcon } from '@chakra-ui/icons';
import { useContext } from 'react';
import { SearchContext } from '../context/searchContext';

export const Header = () => {
  const { search, handleSearch } = useContext(SearchContext);

  return (
    <GridItem
      colStart={3}
      colEnd={-1}
      borderBottom='1px solid black'
      display='flex'
      alignItems='center'
    >
      <InputGroup w='40%'>
        <InputLeftElement
          pointerEvents='none'
          children={<SearchIcon color='#B3B5B9' />}
        />
        <Input
          type='text'
          placeholder='Search name'
          border='none'
          value={search}
          onChange={(e) => handleSearch(e.target.value)}
        />
      </InputGroup>
    </GridItem>
  );
};
