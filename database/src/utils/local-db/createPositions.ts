import { allPositions, Position } from 'src/local-models';

export function createPositions(): Position[] {
  const results = [] as Position[];
  allPositions.forEach((name) => {
    const item = new Position(name);
    results.push(item);
  });
  return results;
}
