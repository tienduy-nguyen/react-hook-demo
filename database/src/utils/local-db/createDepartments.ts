import { allDepartments, Department, Position } from 'src/local-models';

export function createDepartments(): Department[] {
  const results = [] as Position[];
  allDepartments.forEach((name) => {
    const item = new Position(name);
    results.push(item);
  });
  return results;
}
