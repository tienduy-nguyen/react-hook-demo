import { Department, Gender, Position, Staff } from 'src/local-models';
import { DB } from '.';
import faker from 'faker';

export function createStaffs(quantity: number, originDb: DB): Staff[] {
  const staffs = [] as Staff[];
  for (let i = 0; i < quantity; ++i) {
    const full_name = `${faker.name.firstName()}  ${faker.name.lastName()}`;
    const birth_day = faker.date.past();
    const gender = randomItem<Gender>([Gender.Male, Gender.Female]);
    const phone_number = faker.phone.phoneNumber();
    const address = faker.address.streetAddress();
    const province = faker.address.city();
    const position_id = randomItem<Position>(originDb.positions)._id;
    const department_id = randomItem<Department>(originDb.departments)._id;

    const staff = new Staff(
      full_name,
      birth_day,
      gender,
      phone_number,
      address,
      province,
      position_id,
      department_id
    );
    staffs.push(staff);
  }
  return staffs;
}

function randomItem<T>(positions: T[]): T {
  const randomIndex = Math.floor(Math.random() * positions.length);
  return positions[randomIndex];
}
