import {
  createDepartments,
  createPositions,
  createStaffs,
  DB,
  saveDatabase,
} from '.';
import fs from 'fs';

export async function main() {
  let db: DB = JSON.parse(fs.readFileSync('src/data/db.json', 'utf8'));

  if (db.positions?.length < 1) {
    const positions = createPositions();
    db.positions = positions;
  }

  if (db.departments?.length < 1) {
    const departments = createDepartments();
    db.departments = departments;
  }

  const staffs = createStaffs(100, db);
  db.staffs = staffs;

  await saveDatabase(db);
  console.log('Data created!');
}
