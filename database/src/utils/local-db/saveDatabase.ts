import { Department, Position, Staff } from 'src/local-models';
import mongoose from 'mongoose';
const { writeFile } = require('fs').promises;

export interface DBInput {
  positions?: Position[];
  departments?: Department[];
  staffs?: Staff[];
}
export interface DB {
  positions: Position[];
  departments: Department[];
  staffs: Staff[];
}

export async function saveDatabase(
  data: DBInput,
  pathName = 'src/data/db.json'
) {
  const json = JSON.stringify(data);
  await writeFile(pathName, json);
}

export function addBatchItems(data: DB, originDb: DB) {
  const { positions, departments, staffs } = data;
  if (positions) {
    positions.forEach((item) => originDb.positions.push(item));
  }
  if (departments) {
    departments.forEach((item) => originDb.departments.push(item));
  }
  if (staffs) {
    staffs.forEach((item) => originDb.staffs.push(item));
  }
}

export function updateBatchItem(data: DB, originDb: DB) {
  const { positions, departments, staffs } = data;
  if (positions) {
    updateObjectsDb(positions, originDb);
  }
  if (departments) {
    updateObjectsDb(departments, originDb);
  }
  if (staffs) {
    updateObjectsDb(staffs, originDb);
  }
}

function updateObjectsDb<T extends { _id?: mongoose.Types.ObjectId }>(
  obj: T[],
  originDb: DB
) {
  obj.forEach((item) => {
    originDb.positions.map((origin) =>
      origin._id === item._id ? item : origin
    );
  });
}
