import faker from 'faker';
import { allDepartments, allPositions } from 'src/local-models';
import { Department, Gender, IStaff, Position, Staff } from 'src/models';

export async function createFakeStaffs(quantity: number) {
  const staffs = [] as IStaff[];
  for (let i = 0; i < quantity; ++i) {
    const full_name = `${faker.name.firstName()}  ${faker.name.lastName()}`;
    const birth_day = faker.date.past();
    const gender = randomItem<Gender>([Gender.male, Gender.female]);
    const phone_number = faker.phone.phoneNumber();
    const address = faker.address.streetAddress();
    const province = faker.address.city();

    const position_name = randomItem<string>(allPositions);
    const positionObj = await Position.findOne({ name: position_name });
    const department_name = randomItem<string>(allDepartments);
    const departmentObj = await Department.findOne({ name: department_name });

    const newStaff = await Staff.create({
      full_name,
      birth_day,
      gender,
      phone_number,
      address,
      province,
      position: positionObj._id,
      department: departmentObj._id,
    });
    staffs.push(newStaff);
  }
  return staffs;
}

function randomItem<T>(positions: T[]): T {
  const randomIndex = Math.floor(Math.random() * positions.length);
  return positions[randomIndex];
}
