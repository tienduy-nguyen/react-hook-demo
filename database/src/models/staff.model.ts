import mongoose, { Schema, Document } from 'mongoose';
import { IDepartment } from './department.model';
import { IPosition } from './position.model';

export enum Gender {
  male = 'Male',
  female = 'Female',
  undisclosed = 'Undisclosed',
}

export interface IStaff extends Document {
  full_name: string;
  birth_day: Date;
  gender: Gender;
  phone_number: string;
  address: string;
  province: string;
  position: IPosition['_id'];
  department_id?: IDepartment['_id'];
}

const UserSchema: Schema = new Schema(
  {
    full_name: { type: String, required: true },
    birth_day: { type: Date, required: true },
    gender: { type: String, enum: Object.values(Gender), default: Gender.male },
    phone_number: { type: String },
    address: { type: String },
    province: { type: String },
    position: { type: Schema.Types.ObjectId, required: true },
    department: { type: Schema.Types.ObjectId, required: true },
  },
  {
    timestamps: true,
  }
);

export const Staff = mongoose.model<IStaff>('staffs', UserSchema);
