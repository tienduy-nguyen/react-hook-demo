import mongoose, { Schema, Document } from 'mongoose';

export interface IDepartment extends Document {
  name: string;
}

const DepartmentSchema: Schema = new Schema({
  name: { type: String, required: true, unique: true },
});

export const Department = mongoose.model<IDepartment>(
  'departments',
  DepartmentSchema
);
