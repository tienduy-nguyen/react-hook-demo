import mongoose, { Schema, Document } from 'mongoose';

export interface IPosition extends Document {
  name: string;
}

const PositionSchema: Schema = new Schema({
  name: { type: String, required: true, unique: true },
});

export const Position = mongoose.model<IPosition>('positions', PositionSchema);
