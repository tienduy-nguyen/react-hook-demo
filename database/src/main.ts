import dotenv from 'dotenv';
import express, { Application } from 'express';
import { allDepartments, allPositions } from './local-models';
import { Department, Position } from './models';
import { connectDb } from './connect';
import { createFakeStaffs } from './utils/createFakeStaff';

async function main() {
  dotenv.config();

  // Connect db

  // Create dummy data

  try {
    await connectDb();
    for (let name of allPositions) {
      const exists = await Position.findOne({ name });
      if (!exists) {
        await Position.create({
          name,
        });
      }
    }
    for (let name of allDepartments) {
      const exists = await Department.findOne({ name });
      if (!exists) {
        await Department.create({
          name,
        });
      }
    }
    await createFakeStaffs(100);
  } catch (error) {
    console.error(error.message);
    process.exit(1);
  }

  const app: Application = express();
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  app.get('/', (req, res) => {
    res.send('Hi there!');
  });

  app.listen(3006, () => {
    console.log('Server is running at http://localhost:3006/');
  });
}

main().catch((err) => console.log(err));
