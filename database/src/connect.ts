import { Db } from 'mongodb';
import mongoose from 'mongoose';

export const connectDb = async (): Promise<Db> => {
  let cloudDb: Db;
  try {
    const mongoClient = await mongoose.connect(process.env.MONGO_URI, {
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    console.log('MongoDB has been connected');
    cloudDb = mongoClient.connection.db;
    return cloudDb;
  } catch (err) {
    console.error(err.message);
    process.exit(1);
  }
};
