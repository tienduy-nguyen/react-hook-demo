import mongoose from 'mongoose';

export class Department {
  public _id: mongoose.Types.ObjectId;

  constructor(public name: string) {
    this._id = mongoose.Types.ObjectId();
  }
}

export const allDepartments = [
  'Services',
  'Accounting',
  'Training',
  'R&D',
  'Engineering',
  'Legal',
  'HR',
];
