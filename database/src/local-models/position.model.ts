import mongoose from 'mongoose';

export class Position {
  public _id: mongoose.Types.ObjectId;

  constructor(public name: string) {
    this._id = mongoose.Types.ObjectId();
  }
}

export const allPositions = [
  'Financial Analyst',
  'Nurse Practicioner',
  'Statistician I',
  'Human Resources Assistant I',
  'Human Resources Assistant II',
  'Human Resources Assistant III',
  'General Manager',
  'Database Administrator I',
  'Database Administrator II',
  'Database Administrator III',
  'Project Manager',
  'Marketing Manager',
  'Paralegal',
  'Automation Specialist I',
  'Automation Specialist II',
  'Automation Specialist III',
];
