import mongoose from 'mongoose';

export class Staff {
  _id: mongoose.Types.ObjectId;

  constructor(
    public full_name: string,
    public birth_day: Date,
    public gender: Gender,
    public phone_number: string,
    public address: string,
    public province: string,
    public position_id: mongoose.Types.ObjectId,
    public department_id: mongoose.Types.ObjectId
  ) {
    this._id = mongoose.Types.ObjectId();
  }
}

export enum Gender {
  'Male',
  'Female',
}
