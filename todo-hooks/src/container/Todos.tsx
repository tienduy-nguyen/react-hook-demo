import * as React from 'react';
import { Todo } from 'src/components/Todo';
import { ITodo, TodoContext, TodoContextType } from 'src/context/todoContext';

export const Todos = () => {
  const { todos, updateTodo } = React.useContext(
    TodoContext
  ) as TodoContextType;
  return (
    <>
      {todos.map((todo: ITodo) => (
        <Todo key={todo.id} updateTodo={updateTodo} todo={todo} />
      ))}
    </>
  );
};
