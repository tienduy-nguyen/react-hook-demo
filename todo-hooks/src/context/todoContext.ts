import { createContext } from 'react';

export interface ITodo {
  id: number;
  title: string;
  description: string;
  status: boolean;
}

export interface TodoContextType {
  todos: ITodo[];
  saveTodo: (todo: ITodo) => void;
  updateTodo: (id: number) => void;
}

export const TodoContext = createContext<TodoContextType | null>(null);
