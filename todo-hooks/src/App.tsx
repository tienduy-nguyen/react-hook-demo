import React, { useState } from 'react';
import './App.css';
import { AddTodo } from './components/AddTodo';
import { Todos } from './container/Todos';
import { TodoProvider } from './store/todoProvider';

function App() {
  return (
    <TodoProvider>
      <main className='App'>
        <h1>My Todos</h1>
        <AddTodo />
        <Todos />
      </main>
    </TodoProvider>
  );
}

export default App;
