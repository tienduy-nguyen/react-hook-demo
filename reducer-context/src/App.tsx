import React from 'react';
import './App.css';
import { AppProvider } from './context/appContext';
import { Products } from './containers/Products';
import { ShoppingCart } from './containers/ShoppingCart';
import { CreateProduct } from './components/CreateProduct';

function App() {
  return (
    <AppProvider>
      <main className='App'>
        <h1>useReducer - useContext</h1>
        <ShoppingCart />
        <CreateProduct />
        <Products />
      </main>
    </AppProvider>
  );
}

export default App;
