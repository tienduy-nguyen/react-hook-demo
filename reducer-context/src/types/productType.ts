export type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key;
      }
    : {
        type: Key;
        payload: M[Key];
      };
};

export enum Types {
  Create = 'CREATE_PRODUCT',
  Delete = 'DELETE_PRODUCT',
  Add = 'ADD_PRODUCT',
}

// Product

export interface ProductType {
  id?: number | string;
  name: string;
  price: number | string;
}

export type ProductPayload = {
  [Types.Create]: {
    id?: number | string;
    name: string;
    price: number | string;
  };
  [Types.Delete]: {
    id: number | string;
  };
};

export type ProductActions = ActionMap<ProductPayload>[keyof ActionMap<ProductPayload>];

export type ShoppingCartPayload = {
  [Types.Add]: undefined;
};

export type ShoppingCartActions = ActionMap<ShoppingCartPayload>[keyof ActionMap<ShoppingCartPayload>];

export type InitialStateType = {
  products: ProductType[];
  shoppingCart: number;
};
