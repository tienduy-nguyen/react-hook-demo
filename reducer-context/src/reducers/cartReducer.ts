import {
  ProductActions,
  ShoppingCartActions,
  Types,
} from 'src/types/productType';

export const cartReducer = (
  state: number,
  action: ProductActions | ShoppingCartActions
) => {
  switch (action.type) {
    case Types.Add:
      return state + 1;
    default:
      return state;
  }
};
