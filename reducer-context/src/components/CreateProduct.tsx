import React from 'react';
import { AppContext } from 'src/context/appContext';
import { useForm } from 'src/hooks/useForm';
import { ProductType, Types } from 'src/types/productType';

export const CreateProduct = () => {
  const { dispatch } = React.useContext(AppContext);
  const initialForm = { name: '', price: 0 };
  const [formData, setFormData] = React.useState<ProductType>(initialForm);

  const handleForm = (e: React.FormEvent<HTMLInputElement>): void => {
    setFormData({
      ...formData,
      [e.currentTarget.id]: e.currentTarget.value,
    });
  };

  const handleSaveProduct = (
    e: React.FormEvent,
    formData: ProductType | any
  ) => {
    e.preventDefault();
    dispatch({
      type: Types.Create,
      payload: {
        id: Math.round(Math.random() * 10000),
        name: formData?.name,
        price: formData?.price,
      },
    });

    setFormData(initialForm);
  };

  return (
    <form className='Form' onSubmit={(e) => handleSaveProduct(e, formData)}>
      <div>
        <div>
          <label htmlFor='name'>Title</label>
          <input
            onChange={handleForm}
            type='text'
            id='name'
            name='name'
            value={formData?.name}
          />
        </div>
        <div>
          <label htmlFor='price'>Price</label>
          <input
            onChange={handleForm}
            type='text'
            id='price'
            name='price'
            value={formData?.price}
          />
        </div>
      </div>
      <button disabled={formData.name.trim().length < 1 ? true : false}>
        Add Product
      </button>
    </form>
  );
};
