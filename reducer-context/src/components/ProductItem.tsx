import React, { useContext } from 'react';
import { AppContext } from 'src/context/appContext';
import { ProductType, Types } from 'src/types/productType';

type Props = {
  product: ProductType;
};
export const ProductItem: React.FC<Props> = ({ product }) => {
  const { dispatch } = useContext(AppContext);

  const deleteProduct = (id: any) => {
    dispatch({
      type: Types.Delete,
      payload: { id },
    });
  };
  return (
    <div className='Card'>
      <div className='Card--text'>
        <h1>{product.name}</h1>
        <span>{product.price}</span>
      </div>
      <button
        onClick={() => deleteProduct(product.id)}
        className='Card--button'
      >
        Delete
      </button>
    </div>
  );
};
