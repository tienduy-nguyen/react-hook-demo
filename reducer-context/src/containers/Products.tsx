import React from 'react';
import { ProductItem } from 'src/components/ProductItem';
import { AppContext } from 'src/context/appContext';

interface ProductsProps {}

export const Products: React.FC<ProductsProps> = ({}) => {
  const { state } = React.useContext(AppContext);
  const { products } = state;
  return (
    <>
      {products.map((product) => (
        <ProductItem key={product.id} product={product} />
      ))}
    </>
  );
};
