import * as React from 'react';
import { AppContext } from 'src/context/appContext';
import { Types } from 'src/types/productType';

export const ShoppingCart = () => {
  const { state, dispatch } = React.useContext(AppContext);

  return (
    <div className='Form'>
      <button
        onClick={() => {
          dispatch({
            type: Types.Add,
          });
        }}
      >
        click
      </button>
      {state.shoppingCart}
    </div>
  );
};
