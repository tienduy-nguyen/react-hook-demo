import React, { createContext, Dispatch, useReducer } from 'react';
import {
  InitialStateType,
  ProductActions,
  ShoppingCartActions,
} from 'src/types/productType';
import { productReducer, cartReducer } from 'src/reducers';

const initialState = {
  products: [
    {
      id: 1,
      name: 'IPhone 11 Pro Max',
      price: '1100',
    },
    {
      id: 2,
      name: 'IPhone 12 Pro Max',
      price: '1200',
    },
    {
      id: 3,
      name: 'IPhone 13 Pro Max',
      price: '1300',
    },
  ],
  shoppingCart: 0,
};

export const AppContext = createContext<{
  state: InitialStateType;
  dispatch: Dispatch<ProductActions | ShoppingCartActions>;
}>({
  state: initialState,
  dispatch: () => null,
});

const mainReducer = (
  { products, shoppingCart }: InitialStateType,
  action: ProductActions | ShoppingCartActions
) => ({
  products: productReducer(products, action),
  shoppingCart: cartReducer(shoppingCart, action),
});

export const AppProvider: React.FC<React.ReactNode> = ({ children }) => {
  const [state, dispatch] = useReducer(mainReducer, initialState);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
};
