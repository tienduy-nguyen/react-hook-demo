import React from 'react';

export interface User {
  id?: string | number;
  username?: string;
}
export interface IUserContext {
  user: User | null;
  login: (user: User) => void;
  logout: () => void;
}

export const UserContext = React.createContext<IUserContext | null>(null);
