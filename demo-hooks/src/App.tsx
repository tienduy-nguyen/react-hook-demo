import React, { useMemo, useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Navbar } from './components/Navbar';
import { UserContext } from './context/userContext';
import { Home } from './pages/Home';
import { Form } from './pages/Form';
import { About } from './pages/About';
import { UserProvider } from './store/userProvider';

function App() {
  const [user, setUser] = useState(null);

  const value = useMemo(() => ({ user, setUser }), [user, setUser]);

  return (
    <Router>
      <div className='App'>
        <UserProvider>
          <Navbar />
          <Route path='/' exact component={Home} />
          <Route path='/form' exact component={Form} />
          <Route path='/about' exact component={About} />
        </UserProvider>
      </div>
    </Router>
  );
}

export default App;
