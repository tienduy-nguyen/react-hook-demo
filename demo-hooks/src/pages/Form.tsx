import React from 'react';
import { useForm } from '../hooks/useForm';

export function Form() {
  const [formData, setFormData] = useForm({ email: '', password: '' });

  return (
    <div className='box'>
      <form
        className='form'
        onSubmit={(e) => {
          e.preventDefault();
          console.log(formData);
        }}
      >
        <label>Email</label>
        <input
          className='input'
          type='email'
          name='email'
          placeholder='Email'
          value={formData.email}
          onChange={setFormData}
        ></input>
        <label>Password</label>
        <input
          className='input'
          type='password'
          name='password'
          placeholder='Password'
          value={formData.password}
          onChange={setFormData}
        ></input>
        <button type='submit' className='btn'>
          Submit
        </button>
      </form>
    </div>
  );
}
