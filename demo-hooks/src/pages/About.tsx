import React, { useContext } from 'react';
import { IUserContext, UserContext } from 'src/context/userContext';

export function About() {
  const { user, setUser } = useContext(UserContext) as IUserContext;

  return (
    <div className='container'>
      <h2>About</h2>
      <pre>{JSON.stringify(user, null, 2)}</pre>
    </div>
  );
}
