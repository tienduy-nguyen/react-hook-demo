import React from 'react';
import { Link } from 'react-router-dom';
import { IUserContext, UserContext } from 'src/context/userContext';
import { useLogin } from 'src/hooks/useLogin';

export function Navbar() {
  const { user, login, logout } = React.useContext(UserContext) as IUserContext;

  return (
    <div className='App-header'>
      <div className='flex'>
        <h1 className='link'>
          <Link className='link' to='/'>
            Home
          </Link>
        </h1>
        <h1 className='link'>
          <Link className='link' to='/about'>
            About
          </Link>
        </h1>
        <h1 className='link'>
          <Link className='link' to='/form'>
            Form
          </Link>
        </h1>
      </div>
      <div className='flex'>
        {user ? (
          <>
            <button className='btn transparent'>Hi {user?.username}</button>
            <button className='btn' onClick={() => logout()}>
              Logout
            </button>
          </>
        ) : (
          <button className='btn' onClick={() => login(useLogin())}>
            Login
          </button>
        )}
      </div>
    </div>
  );
}
