import React, { Children, useState } from 'react';
import { User, UserContext } from 'src/context/userContext';

export const UserProvider: React.FC<React.ReactNode> = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);

  const login = (user: User) => {
    setUser(user);
  };
  const logout = () => {
    setUser(null);
  };

  return (
    <UserContext.Provider value={{ user, login, logout }}>
      {children}
    </UserContext.Provider>
  );
};
